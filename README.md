# k8s-gitlab-demo

This project creates a [kind](https://kind.sigs.k8s.io/) cluster, installs
[GitLab](https://about.gitlab.com/) using the
[Helm charts](https://charts.gitlab.io/), and configures various demo-related
resources in the local GitLab.

## Getting started

To start, run `./start`.  To remove the cluster, run `./stop`.

## Configuration

The following environment variables can be set to configure the behavior of the
deployed environment:

| Environment variable | Default value                           |
| -------------------- | -------------                           |
| `GITLAB_IP`          | IP address of primary network interface |
| `GITLAB_DOMAIN`      | `$GITLAB_IP.nip.io`                     |
| `GITLAB_URL`         | `https://gitlab.$GITLAB_DOMAIN/`        |

To configure local environment variables persistently, add them to the file
`local_env` in the current directory.
