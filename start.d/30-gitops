#!/bin/bash

set -e

. lib/common

# Create the demo project
demo_group=containers
out=$( gl -o json group list | jq '.[] | select(.name == "'"$demo_group"'")' )
demo_group_id="$( echo "$out" | jq -r .id )"

demo_project=nginx-demo
out=$( gl -o json project list | jq ".[] | select(.path_with_namespace == \"${demo_group}/${demo_project}\")" )
if [ -n "$out" ] ; then
    warn "${demo_group}/${demo_project} already created"
else
    out=$( gl -o json project create --name "${demo_project}" --namespace "$demo_group_id" --visibility public )
    if [ "$?" -ne 0 ] ; then
        die "Failed to create ${demo_group}/${demo_project}"
    fi
fi
demo_project_id="$( echo "$out" | jq -r .id )"

# Populate the demo project
mkdir -p .tmp
tmpdir=$( mktemp -d ".tmp/${demo_project}.XXXX" )
trap "rm -rf ${tmpdir}" EXIT

url=$( echo "$out" | jq -r .http_url_to_repo )
method="${url%%://*}"
fullpath="${url#*://}"
token=$( cat .root_token )
clone_url="${method}://Automation%20token:${token}@${fullpath}"

git clone "${clone_url}" "${tmpdir}"
cp -af demo-project/* demo-project/.[^.]* "${tmpdir}"/
sed -i \
    -e 's,\([[:space:]]\)nginx:stable$,\1registry.'"${GITLAB_DOMAIN}/${demo_group}/${demo_project}:latest," \
    -e 's,\([[:space:]]\)\(nginx\.\)_domain_$,\1\2'"${GITLAB_DOMAIN}," \
    "${tmpdir}"/manifests/*.yaml
mkdir -p "${tmpdir}/.gitlab/agents/${demo_project}"
cat > "${tmpdir}/.gitlab/agents/${demo_project}/config.yaml" <<END_CONFIG
---
gitops:
  manifest_projects:
    - id: ${demo_group}/${demo_project}
      inventory_policy: adopt_if_no_inventory
      default_namespace: default
      paths:
        - glob: '/manifests/**/*.yaml'
END_CONFIG
git -C "${tmpdir}" add -A
if [ -n "$( git -C "${tmpdir}" status --short )" ] ; then
    git -C "${tmpdir}" commit -m "Import from $( basename "$0" )"
fi
git -C "${tmpdir}" push origin "$( git -C "${tmpdir}" symbolic-ref --short HEAD )"

rm -rf "${tmpdir}"
trap - EXIT

# Register the agent
# The `gitlab` client does not support this (yet),
# so we have to use `curl` to talk to the API.
out=$( curl -f -s -H "Private-Token: ${token}" "${GITLAB_URL}/api/v4/projects/${demo_project_id}/cluster_agents" )
out=$( echo "$out" | jq ".[] | select(.config_project.path_with_namespace == \"${demo_group}/${demo_project}\")" )
if [ -n "$( echo "$out" | jq -r '.id' )" ] ; then
    warn "Re-using cluster agent"
else
    out=$( curl -f -s \
        -H "Private-Token: ${token}" \
        -H 'Content-Type: application/json' \
        -X POST \
        --data "{\"name\":\"${demo_project}\"}" \
        "${GITLAB_URL}/api/v4/projects/${demo_project_id}/cluster_agents" )
fi
agent_id=$( echo "$out" | jq -r '.id' )

# Create a token
out=$( curl -f -s \
    -H "Private-Token: ${token}" \
    -H 'Content-Type: application/json' \
    -X POST \
    --data "{\"name\":\"${demo_project}\"}" \
    "${GITLAB_URL}/api/v4/projects/${demo_project_id}/cluster_agents/${agent_id}/tokens" )
agent_token=$( echo "$out" | jq -r .token )

# Install the agent
helm upgrade --install "${demo_project}" gitlab/gitlab-agent \
    --set config.token="${agent_token}" \
    --set config.kasAddress="wss://kas.${GITLAB_DOMAIN}"
    # --namespace gitlab-agent \
    # --create-namespace \
    # --set image.tag=v15.5.1 \
